#include "Background.h"
#include <gl/glew.h>
#include <gl/glfw.h>
#include <iostream>

namespace Exoterra
{
	namespace Client
	{
		namespace Rendering
		{

			Background::Background(Texture *far, Texture *near, float farParallax)
			{
				mFar = far;
				mNear = near;
				mParallax = farParallax;

				//First, we need to adjust our texture.
				glBindTexture(GL_TEXTURE_2D, mFar->GLID);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
				glBindTexture(GL_TEXTURE_2D, mNear->GLID);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
			}

			void Background::Initialize(std::string VertShader, std::string FragShader)
			{
				mValid = true;

				int shader_ok;

				//First, create and compile a vertex shader.
				unsigned int v_shader = glCreateShader(GL_VERTEX_SHADER);
				const char *src = VertShader.c_str();
				int length = VertShader.length();
				glShaderSource(v_shader, 1, &src, &length);
				glCompileShader(v_shader);
				glGetShaderiv(v_shader, GL_COMPILE_STATUS, &shader_ok);
				//Then check if it compiled okay.
				if (!shader_ok)
				{
					//If not, say so, delete it, and leave.
					mValid = false;

					std::cout<<"Shader failed to compile."<<std::endl;
					int log_length;
					char *log;
					glGetShaderiv(v_shader, GL_INFO_LOG_LENGTH, &log_length);
					log = new char[log_length];
					glGetShaderInfoLog(v_shader, log_length, 0, log);
					std::cout<<log<<std::endl;
					delete log;

					glDeleteShader(v_shader);
					return;
				}
				
				//Do the same for the fragment shader.
				unsigned int f_shader = glCreateShader(GL_FRAGMENT_SHADER);
				src = FragShader.c_str();
				length = FragShader.length();
				glShaderSource(f_shader, 1, &src, &length);
				glCompileShader(f_shader);
				glGetShaderiv(f_shader, GL_COMPILE_STATUS, &shader_ok);
				if (!shader_ok)
				{
					mValid = false;
					std::cout<<"Shader failed to compile."<<std::endl;
					int log_length;
					char *log;
					glGetShaderiv(f_shader, GL_INFO_LOG_LENGTH, &log_length);
					log = new char[log_length];
					glGetShaderInfoLog(f_shader, log_length, 0, log);
					std::cout<<log<<std::endl;
					delete log;

					glDeleteShader(v_shader);
					glDeleteShader(f_shader);
					return;
				}

				mProgram = glCreateProgram();
				glAttachShader(mProgram, v_shader);
				glAttachShader(mProgram, f_shader);
				glLinkProgram(mProgram);
				int linked;
				glGetProgramiv(mProgram, GL_LINK_STATUS, &linked);
				if (linked == 0)
				{
					mValid = false;
					std::cout<<"Shader failed to link."<<std::endl;
					int log_length;
					char *log;
					glGetProgramiv(mProgram, GL_INFO_LOG_LENGTH, &log_length);
					log = new char[log_length];
					glGetProgramInfoLog(mProgram, log_length, 0, log);
					std::cout<<log<<std::endl;
					delete log;

					glDetachShader(mProgram, v_shader);
					glDetachShader(mProgram, f_shader);
					glDeleteShader(v_shader);
					glDeleteShader(f_shader);
					glDeleteProgram(mProgram);
					return;
				}

				glDeleteShader(v_shader);
				glDeleteShader(f_shader);

				mTexUniform = glGetUniformLocation(mProgram, "texture");

				mProjUniform = glGetUniformLocation(mProgram, "projection");
				mTexSizeUniform = glGetUniformLocation(mProgram, "texsize");

				mTintUniform = glGetUniformLocation(mProgram, "tint");
				mScreenSizeUniform = glGetUniformLocation(mProgram, "screensize");
				mCamPosUniform = glGetUniformLocation(mProgram, "campos");
				mParallaxUniform = glGetUniformLocation(mProgram, "parallax");

				mPosAttr = glGetAttribLocation(mProgram, "position");

				float verts[12] = {
					0.0f, 0.0f, 0.0f,
					1.0f, 0.0f, 0.0f,
					0.0f, 1.0f, 0.0f,
					1.0f, 1.0f, 0.0f
				};

				glGenVertexArrays(1, &mVAO);
				glBindVertexArray(mVAO);

				glGenBuffers(1, &mBuffer);

				glBindBuffer(GL_ARRAY_BUFFER, mBuffer);
				glBufferData(GL_ARRAY_BUFFER, 12 * sizeof(float), verts, GL_STATIC_DRAW);
				glVertexAttribPointer(mPosAttr, 3, GL_FLOAT, GL_FALSE, 0, 0);
				glEnableVertexAttribArray(mPosAttr);

				glBindVertexArray(0);
				glBindBuffer(GL_ARRAY_BUFFER, 0);
			}

			void Background::Render(Math::Vector2 &camPos, int w, int h, float *projection, float red, float green, float blue, float alpha)
			{
				glUseProgram(mProgram);

				glActiveTexture(GL_TEXTURE0);
				glBindTexture(GL_TEXTURE_2D, mFar->GLID);
				glUniform1i(mTexUniform, 0);

				glUniformMatrix4fv(mProjUniform, 16, false, projection);
				glUniform2f(mTexSizeUniform, (float)mFar->Width, (float)mFar->Height);
				glUniform2f(mCamPosUniform, camPos.GetX(), camPos.GetY());
				glUniform4f(mTintUniform, red, green, blue, alpha - 0.1f);
				glUniform2f(mScreenSizeUniform, (float)w, (float)h);

				glUniform1f(mParallaxUniform, mParallax);


				glBindVertexArray(mVAO);
				glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

				glActiveTexture(GL_TEXTURE0);
				glBindTexture(GL_TEXTURE_2D, mNear->GLID);
				glUniform1i(mTexUniform, 0);
				glUniform1f(mParallaxUniform, 1.0f);
				glUniform4f(mTintUniform, red, green, blue, alpha);
				
				glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

				glBindVertexArray(0);
				glUseProgram(0);
			}

		}
	}
}