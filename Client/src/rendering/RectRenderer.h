#pragma once
#include <string>

#include "../math/Vector2.h"
#include "../Texture.h"

namespace Exoterra
{
	namespace Client
	{
		namespace Rendering
		{

			class RectRenderer
			{
			protected:
				unsigned int mProgram;
				unsigned int mBuffer;
				unsigned int mVAO;

				int mTexUniform;

				int mProjUniform;
				int mTexSizeUniform;

				int mPosUniform;
				int mOffsetUniform;
				int mAngleUniform;

				int mPosAttr;

				bool mValid;
			public:
				RectRenderer();
				virtual ~RectRenderer();

				void Initialize(std::string VertShader, std::string FragShader);
				
				void DrawRect(Texture *texture, Math::Vector2 &position, Math::Vector2 &offset, float *projection, float angle);

				bool IsValid() { return mValid; }
			};
		}
	}
}