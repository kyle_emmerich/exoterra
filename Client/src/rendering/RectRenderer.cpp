#include "RectRenderer.h"
#include "GL/glew.h"
#include <iostream>

namespace Exoterra
{
	namespace Client
	{
		namespace Rendering
		{
			RectRenderer::RectRenderer()
			{
			}

			RectRenderer::~RectRenderer()
			{
				glDeleteBuffers(1, &mBuffer);
				glDeleteProgram(mProgram);
			}

			void RectRenderer::Initialize(std::string VertShader, std::string FragShader)
			{
				mValid = true;

				int shader_ok;

				//First, create and compile a vertex shader.
				unsigned int v_shader = glCreateShader(GL_VERTEX_SHADER);
				const char *src = VertShader.c_str();
				int length = VertShader.length();
				glShaderSource(v_shader, 1, &src, &length);
				glCompileShader(v_shader);
				glGetShaderiv(v_shader, GL_COMPILE_STATUS, &shader_ok);
				//Then check if it compiled okay.
				if (!shader_ok)
				{
					//If not, say so, delete it, and leave.
					mValid = false;

					std::cout<<"Shader failed to compile."<<std::endl;
					int log_length;
					char *log;
					glGetShaderiv(v_shader, GL_INFO_LOG_LENGTH, &log_length);
					log = new char[log_length];
					glGetShaderInfoLog(v_shader, log_length, 0, log);
					std::cout<<log<<std::endl;
					delete log;

					glDeleteShader(v_shader);
					return;
				}
				
				//Do the same for the fragment shader.
				unsigned int f_shader = glCreateShader(GL_FRAGMENT_SHADER);
				src = FragShader.c_str();
				length = FragShader.length();
				glShaderSource(f_shader, 1, &src, &length);
				glCompileShader(f_shader);
				glGetShaderiv(f_shader, GL_COMPILE_STATUS, &shader_ok);
				if (!shader_ok)
				{
					mValid = false;
					std::cout<<"Shader failed to compile."<<std::endl;
					int log_length;
					char *log;
					glGetShaderiv(f_shader, GL_INFO_LOG_LENGTH, &log_length);
					log = new char[log_length];
					glGetShaderInfoLog(f_shader, log_length, 0, log);
					std::cout<<log<<std::endl;
					delete log;

					glDeleteShader(v_shader);
					glDeleteShader(f_shader);
					return;
				}

				mProgram = glCreateProgram();
				glAttachShader(mProgram, v_shader);
				glAttachShader(mProgram, f_shader);
				glLinkProgram(mProgram);
				int linked;
				glGetProgramiv(mProgram, GL_LINK_STATUS, &linked);
				if (linked == 0)
				{
					mValid = false;
					std::cout<<"Shader failed to link."<<std::endl;
					int log_length;
					char *log;
					glGetProgramiv(mProgram, GL_INFO_LOG_LENGTH, &log_length);
					log = new char[log_length];
					glGetProgramInfoLog(mProgram, log_length, 0, log);
					std::cout<<log<<std::endl;
					delete log;

					glDetachShader(mProgram, v_shader);
					glDetachShader(mProgram, f_shader);
					glDeleteShader(v_shader);
					glDeleteShader(f_shader);
					glDeleteProgram(mProgram);
					return;
				}

				glDeleteShader(v_shader);
				glDeleteShader(f_shader);

				mTexUniform = glGetUniformLocation(mProgram, "texture");

				mProjUniform = glGetUniformLocation(mProgram, "projection");
				mTexSizeUniform = glGetUniformLocation(mProgram, "texsize");

				mPosUniform = glGetUniformLocation(mProgram, "pos");
				mOffsetUniform = glGetUniformLocation(mProgram, "offset");
				mAngleUniform = glGetUniformLocation(mProgram, "angle");

				mPosAttr = glGetAttribLocation(mProgram, "position");

				float verts[12] = {
					0.0f, 0.0f, 0.0f,
					1.0f, 0.0f, 0.0f,
					0.0f, 1.0f, 0.0f,
					1.0f, 1.0f, 0.0f
				};

				glGenVertexArrays(1, &mVAO);
				glBindVertexArray(mVAO);

				glGenBuffers(1, &mBuffer);

				glBindBuffer(GL_ARRAY_BUFFER, mBuffer);
				glBufferData(GL_ARRAY_BUFFER, 12 * sizeof(float), verts, GL_STATIC_DRAW);
				glVertexAttribPointer(mPosAttr, 3, GL_FLOAT, GL_FALSE, 0, 0);
				glEnableVertexAttribArray(mPosAttr);

				glBindVertexArray(0);
				glBindBuffer(GL_ARRAY_BUFFER, 0);
			}

			void RectRenderer::DrawRect(Texture *texture, Math::Vector2 &position, Math::Vector2 &offset, float *projection, float angle)
			{
				glUseProgram(mProgram);

				glActiveTexture(GL_TEXTURE0);
				glBindTexture(GL_TEXTURE_2D, texture->GLID);
				glUniform1i(mTexUniform, 0);

				glUniformMatrix4fv(mProjUniform, 16, false, projection);
				glUniform2f(mTexSizeUniform, (float)texture->Width, (float)texture->Height);

				glUniform2f(mPosUniform, position.GetX(), position.GetY());
				glUniform2f(mOffsetUniform, offset.GetX(), offset.GetY());
				glUniform1f(mAngleUniform, angle);

				glBindVertexArray(mVAO);
				glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

				glBindVertexArray(0);
				glUseProgram(0);
			}
		}
	}
}