#pragma once

#include "../math/Vector2.h"

namespace Exoterra
{
	namespace Client
	{
		namespace Rendering
		{

			class Camera
			{
			private:

			protected:
				Math::Vector2 mCenter;
			public:
				Camera();
				~Camera();

				void SetCenter(Math::Vector2 &newCenter);
				Math::Vector2 GetCenter();

				Math::Vector2 GetCamPos(int width, int height);
			};

		}
	}
}