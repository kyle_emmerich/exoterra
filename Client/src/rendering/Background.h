#pragma once

#include "RectRenderer.h"

namespace Exoterra
{
	namespace Client
	{
		namespace Rendering
		{

			class Background : public RectRenderer
			{
			private:

			protected:
				Texture *mFar;
				Texture *mNear;
				float mParallax;

				int mTintUniform;
				int mScreenSizeUniform;
				int mCamPosUniform;
				int mParallaxUniform;
			public:
				Background(Texture *far, Texture *near, float farParallax);

				void Initialize(std::string VertShader, std::string FragShader);
				void Render(Math::Vector2 &camPos, int w, int h, float *projection, float redTint = 1.0f, float greenTint = 1.0f, float blueTint = 1.0f, float alphaTint = 1.0f);
			};

		}
	}
}