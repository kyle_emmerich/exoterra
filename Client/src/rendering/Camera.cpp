#include "Camera.h"

namespace Exoterra
{
	namespace Client
	{
		namespace Rendering
		{

			Camera::Camera()
			{
			}

			Camera::~Camera()
			{
			}

			void Camera::SetCenter(Math::Vector2 &newCenter)
			{
				mCenter = newCenter;
			}

			Math::Vector2 Camera::GetCenter()
			{
				return mCenter;
			}

			Math::Vector2 Camera::GetCamPos(int w, int h)
			{
				return (mCenter - Math::Vector2(w * 0.5f, h * 0.5f));
			}

		}
	}
}