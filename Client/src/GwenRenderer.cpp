#include "GwenRenderer.h"
#include "GL/glfw.h"
#include "Gwen/Texture.h"
#include <cmath>
#include <iostream>

namespace Exoterra
{
	namespace Client
	{

		GwenRenderer::GwenRenderer()
		{
			FT_Error error = FT_Init_FreeType(&mFTLibrary);
			if (error) 
			{
				std::cout<<"Couldn't initialize FreeType."<<std::endl;
				//error occurred, handle somehow?
			}
		}

		GwenRenderer::~GwenRenderer()
		{
			FT_Done_FreeType(mFTLibrary);
		}

		void GwenRenderer::LoadFont(Gwen::Font *pFont)
		{
			std::wcout<<"Attempting to load "<<pFont->facename.c_str()<<"."<<std::endl;

			unsigned int *tex = new unsigned int;
			glGenTextures(1, tex);
			glBindTexture(GL_TEXTURE_2D, *tex);

			FontData *fd = new FontData;

			Gwen::String path = Gwen::Utility::UnicodeToString(pFont->facename);
			FT_Face face; FT_New_Face(mFTLibrary, path.c_str(), 0, &face); 

			int tex_width = 256;
			int tex_height = 256;
			int size = tex_width * tex_height;
			unsigned int *data = new unsigned int[size];
			ZeroMemory(&data, sizeof(data));

			Gwen::Point pen;
			FT_GlyphSlot slot = face->glyph;

			int max_w = face->size->metrics.max_advance;
			int max_h = face->max_advance_height;

			fd->CharsPerRow = std::floor((float)tex_width / (float)max_w);
			
			for (int c = 32; c < 126; c++)
			{
				unsigned int glyph_idx;
				glyph_idx = FT_Get_Char_Index(face, c);
				FT_Load_Glyph(face, glyph_idx, FT_LOAD_DEFAULT);
				FT_Render_Glyph(face->glyph, FT_RENDER_MODE_NORMAL);

				fd->Advances[c - 32] = face->glyph->advance.x;

				for (int x = 0; x < slot->bitmap.width; x++)
				{
					for (int y = 0; y < slot->bitmap.rows; y++)
					{
						int pos = x + (y * slot->bitmap.pitch);
						int pos2 = (pen.x + x) + ((pen.y + y) * slot->bitmap.width);
						
						unsigned char r = slot->bitmap.buffer[pos + 0];
						unsigned char g = slot->bitmap.buffer[pos + 1];
						unsigned char b = slot->bitmap.buffer[pos + 2];
						unsigned char a = slot->bitmap.buffer[pos + 3];

						unsigned int col = (r << 24) | (b << 16) | (g << 8) | a;
						data[pos2] = col;
						pen.x += max_w;
						if (pen.x >= 512 - max_w) 
						{
							pen.x = 0;
							pen.y += max_h;
						}
					}
				}
			}


			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, tex_width, tex_height, 0, GL_RGBA, GL_UNSIGNED_INT, data);
			delete [] data;
			FT_Done_Face(face);

			fd->pTexture = new Gwen::Texture;
			fd->pTexture->data = &tex;
			fd->pTexture->width = tex_width;
			fd->pTexture->height = tex_height;

			mFonts.insert(std::pair<Gwen::Font*, FontData*>(pFont, fd));
			std::wcout<<"Loaded font "<<pFont->facename.c_str()<<"."<<std::endl;
		}

		void GwenRenderer::FreeFont(Gwen::Font *pFont)
		{

		}

		void GwenRenderer::RenderText(Gwen::Font *pFont, Gwen::Point Pos, const Gwen::UnicodeString &text)
		{
			Gwen::Point pen;

			Gwen::String converted_string = Gwen::Utility::UnicodeToString(text);

			float fSize = pFont->size * Scale();
			int cpr = mFonts.at(pFont)->CharsPerRow;

			int len = converted_string.length();
			for (int i = 0; i < len; i++) 
			{
				char c = converted_string[i];
				float adv = mFonts.at(pFont)->Advances[c - 32] * fSize;
				Gwen::Rect r(Pos.x + pen.x, Pos.y - fSize * 0.2f, fSize, fSize);

				Gwen::Texture *pTexture = mFonts.at(pFont)->pTexture;
				
				if (pTexture) {
					float uv_texcoords[8] = {0.0f, 0.0f, 1.0f, 1.0f};
					if (c >= 32 && c <= 126)
					{
						float cx = (c % cpr) / (float)cpr;
						float cy = (c / cpr) / (float)cpr;
						uv_texcoords[0] = cx;
						uv_texcoords[1] = cy;
						uv_texcoords[4] = (float)(cx + 1.0f / (float)cpr);
						uv_texcoords[5] = (float)(cy + 1.0f / (float)cpr);
					}
					DrawTexturedRect(pTexture, r, uv_texcoords[0], uv_texcoords[5], uv_texcoords[4], uv_texcoords[1]);
					pen.x += adv;
				}
				else
				{
					DrawFilledRect(r);
					pen.x += adv;
				}
			}
		}

		Gwen::Point GwenRenderer::MeasureText(Gwen::Font *pFont, const Gwen::UnicodeString &text)
		{
			if (mFonts.count(pFont) == 0)
			{
				this->LoadFont(pFont);
			}

			std::wcout<<text.c_str()<<std::endl;

			Gwen::Point p;
			Gwen::String converted_string = Gwen::Utility::UnicodeToString(text);
			int len = text.length();

			float fSize = pFont->size * Scale();
			for (int i = 0; i < len; i++) 
			{
				int c = text.at(i);
				int adv = mFonts.at(pFont)->Advances[c - 32] * fSize;
				p.x += adv;
			}
			return p;
		}
	}
}