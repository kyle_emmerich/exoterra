#include "ShipComponent.h"

namespace Exoterra
{
	namespace Client
	{
		namespace Game
		{
			namespace Ship
			{

				ShipComponent::ShipComponent(ShipComponentDef *def, int ID)
				{
					mType = def->sType;
					mID = ID;
					mDefID = def->sID;

					mSize = def->sSize;
					mInterfaceType = def->sInterfaceType;
					mFormFactor = def->sFormFactor;

					mMass = def->sMass;
				}

				ShipComponent::~ShipComponent()
				{
				}

				ShipComponentType ShipComponent::GetType()
				{
					return mType;
				}

			}
		}
	}
}