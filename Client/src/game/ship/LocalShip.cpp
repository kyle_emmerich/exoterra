#include "LocalShip.h"
#include <cmath>
#include <iostream>

const float PI = 3.14159f;
const float PI_2 = PI / 2;

namespace Exoterra
{
	namespace Client
	{
		namespace Game
		{
			namespace Ship
			{

				LocalShip::LocalShip(ShipTypeDef *def, Math::Vector2 &pos)
					: Ship(def, pos)
				{
				}

				LocalShip::~LocalShip()
				{
				}

				void LocalShip::Update(float delta, ShipInputFrame *input)
				{
					BaseEntity::Update(delta);

					float thrust_power = 0.5f;
					float turn_power = 5.0f;

					if (input->sLeft)
					{
						mAngle -= turn_power * delta;
						mFlightMode = SFM_Free;

						if (mAngle <= -PI)
							mAngle = PI;
					}
					if (input->sRight)
					{
						mAngle += turn_power * delta;
						mFlightMode = SFM_Free;

						if (mAngle >= PI)
							mAngle = -PI;
					}
					
					if (input->sForward)
					{
						Math::Vector2 force;
						force.SetX(std::sin(mAngle));
						force.SetY(-std::cos(mAngle));

						force *= thrust_power;

						LocalApplyForce(force);
						mFlightMode = SFM_Free;
					}

					if (input->sAutopilot && mFlightMode != SFM_Autopilot)
					{
						mFlightMode = SFM_Autopilot;
						mAutopilotStage = SAS_AntithrustRotation;

						mAutoTarget = Math::Vector2(input->sAutopilotX, input->sAutopilotY);
					}

					if (mFlightMode == SFM_Autopilot)
					{
						switch (mAutopilotStage)
						{
						case SAS_Inactive:
							{
								mFlightMode = SFM_Free;
							}
						case SAS_AntithrustRotation:
							{
								if (mVelocity.GetLengthSq() <= 0.0001f) 
								{
									mAutopilotStage = SAS_RotateTo;
									break;
								}
								Math::Vector2 antiVelocity = mVelocity.GetUnit() * -1;
								float targetAng = std::atan2f(antiVelocity.GetX(), -antiVelocity.GetY());

								float diff = LocalTurnTo(targetAng, turn_power, delta);

								if (std::abs(diff) < turn_power * delta * 2.0f)
								{
									mAutopilotStage = SAS_Antithrust;
								}
								break;
							}
						case SAS_Antithrust:
							{
								Math::Vector2 antiVelocity = mVelocity.GetUnit() * -1;
								antiVelocity *= (thrust_power);

								LocalApplyForce(antiVelocity);

								if (mVelocity.GetLengthSq() <= thrust_power * delta * 2.0f)
								{
									mVelocity = Math::Vector2(0.0f, 0.0f);
									mAutopilotStage = SAS_RotateTo;
								}

								break;
							}
						case SAS_RotateTo:
							{
								Math::Vector2 relPos = mAutoTarget - mPos;
								float targetAng = std::atan2f(relPos.GetX(), -relPos.GetY());

								float diff = LocalTurnTo(targetAng, turn_power, delta);

								if (std::abs(diff) < turn_power * delta * 2.0f)
								{
									mAutopilotStage = SAS_Inactive;
								}
							}
						default:
							break;
						}
					}
					
				}

				float LocalShip::LocalTurnTo(float targetAng, float turn_power, float delta)
				{

					int dir = 0;
					float diff = 100.0f;

					if ((mAngle >= 0 && targetAng >= 0) || 
						(mAngle <= 0 && targetAng <= 0))
					{
						//Both are on the same side. Just move directly to it.
						if (mAngle > targetAng) 
						{
							dir = -1;
							diff = mAngle - targetAng;
						}
						if (mAngle < targetAng)
						{
							dir = 1;
							diff = targetAng - mAngle;
						}
					} else {
						//A little more complicated...
						if (mAngle <= 0 && targetAng >= 0)
						{
							//We're pointing left, but need to point right.
							dir = 1;

							//However, going right might not be the quickest way.
							if (std::abs(mAngle) > targetAng)
								dir = -1;
						}

						if (mAngle >= 0 && targetAng <= 0)
						{
							dir = -1;
							if (std::abs(targetAng) < mAngle)
								dir = 1;
						}
					}

					mAngle += dir * turn_power * delta;
					if (dir == 1 && mAngle >= PI)
						mAngle = -PI;
					if (dir == -1 && mAngle <= -PI)
						mAngle = PI;

					return diff;
				}
			}
		}
	}
}