#include "ShipTypeManager.h"

namespace Exoterra
{
	namespace Client
	{
		namespace Game
		{
			namespace Ship
			{

				ShipTypeManager::ShipTypeManager()
				{
				}

				ShipTypeManager::~ShipTypeManager()
				{
					mTypeDefs.clear();
				}

				void ShipTypeManager::AddShipType(ShipTypeDef *typeDef)
				{
					if (mTypeDefs.count(typeDef->sTypeName) != 0)
						return;
					mTypeDefs.insert(std::pair<std::string, ShipTypeDef *>(typeDef->sTypeName, typeDef));
				}

				const ShipTypeDef *ShipTypeManager::LookupShipType(std::string typeName)
				{
					if (mTypeDefs.count(typeName) == 0)
						return 0;
					return mTypeDefs[typeName];
				}


			}
		}
	}
}
