#pragma once

#include <string>

namespace Exoterra
{
	namespace Client
	{
		namespace Game
		{
			namespace Ship
			{

				enum ShipClass
				{
					SC_Invalid = 0,
					SC_Freighter,
					SC_Transport,
					SC_Barge,
					SC_CargoLiner,
					SC_PassengerLiner,
					SC_Fighter,
					SC_Interceptor,
					SC_LongRangeFighter,
					SC_Recon,
					SC_Frigate,
					SC_Support,
					SC_Patrol,
					SC_Destroyer,
					SC_Cruiser,
					SC_Battlecruiser,
					SC_Battleship,
					SC_Carrier,
					SC_Dreadnought,
					SC_MobileWarPlatform
				};
			
				std::string GetShipClassString(ShipClass e);
			}
		}
	}
}