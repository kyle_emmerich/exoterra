#pragma once

namespace Exoterra
{
	namespace Client
	{
		namespace Game
		{
			namespace Ship
			{

				enum ShipAutopilotStage
				{
					SAS_Inactive,
					SAS_AntithrustRotation,
					SAS_Antithrust,
					SAS_RotateTo,
					SAS_Thrust,
					SAS_FinalRotation,
				};

			}
		}
	}
}
