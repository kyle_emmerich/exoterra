#pragma once

namespace Exoterra
{
	namespace Client
	{
		namespace Game
		{
			namespace Ship
			{

				enum ShipFlightMode
				{
					SFM_Free = 0,
					SFM_Autopilot
				};

			}
		}
	}
}
			