#pragma once

#include "ShipComponentType.h"

namespace Exoterra
{
	namespace Client
	{
		namespace Game
		{
			namespace Ship
			{

				typedef struct
				{
					ShipComponentType sType;
					int sID;

					int sSize;
					int sInterfaceType;
					int sFormFactor;

					float sMass;

					size_t sDataSize;
					unsigned char *sData;
				} ShipComponentDef;

			}
		}
	}
}