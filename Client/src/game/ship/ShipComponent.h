#pragma once

#include "ShipComponentDef.h"

namespace Exoterra
{
	namespace Client
	{
		namespace Game
		{
			namespace Ship
			{

				class ShipComponent
				{
				private:

				protected:
					ShipComponentType mType;
					int mID;
					int mDefID;

					int mSize;
					int mInterfaceType;
					int mFormFactor;

					float mMass;
				public:
					ShipComponent(ShipComponentDef *def, int ID);
					~ShipComponent();

					ShipComponentType GetType();
				};

			}
		}
	}
}