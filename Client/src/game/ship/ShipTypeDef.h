#pragma once

#include "../../Texture.h"
#include "ShipComponentSlot.h"
#include "ShipClass.h"

namespace Exoterra
{
	namespace Client
	{
		namespace Game
		{
			namespace Ship
			{

				struct ShipTypeDef
				{
					std::string sTypeName;
					int sTypeID;

					ShipClass sClass;

					Texture *sTexture;
					float sMass;
					
					int sNumComponentSlots;
					ShipComponentSlot *sComponentSlots[];
				};

			}
		}
	}
}