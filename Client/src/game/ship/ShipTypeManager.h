#pragma once

#include "ShipTypeDef.h"
#include <string>
#include <map>
#include <fstream>

namespace Exoterra
{
	namespace Client
	{
		namespace Game
		{
			namespace Ship
			{

				class ShipTypeManager
				{
				private:

				protected:
					std::map<std::string, ShipTypeDef *> mTypeDefs;
				public:
					ShipTypeManager();
					~ShipTypeManager();

					void AddShipType(ShipTypeDef *typeDef);

					const ShipTypeDef *LookupShipType(std::string typeName);
				};

			}
		}
	}
}