#pragma once

#include "Ship.h"
#include "ShipInputFrame.h"

namespace Exoterra
{
	namespace Client
	{
		namespace Game
		{
			namespace Ship
			{

				class LocalShip : public Ship
				{
				private:

				protected:

					unsigned long mLastInput;

				public:
					LocalShip(ShipTypeDef *def, Math::Vector2 &pos);
					~LocalShip();

					void Update(float delta, ShipInputFrame *input);

					float LocalTurnTo(float targetAngle, float power, float delta);
				};

			}
		}
	}
}