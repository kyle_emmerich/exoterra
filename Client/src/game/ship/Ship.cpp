#pragma once

#include "Ship.h"

namespace Exoterra
{
	namespace Client
	{
		namespace Game
		{
			namespace Ship
			{

				Ship::Ship(const ShipTypeDef *def, Math::Vector2 &pos)
					: BaseEntity(def->sTexture, pos, def->sMass)
				{
					for (int i = 0; i < def->sNumComponentSlots; i++)
					{
						ShipComponentSlot *slot = def->sComponentSlots[i];

						mSlots[slot->sID] = slot;
					}


				}

				Ship::~Ship()
				{
				}

				void Ship::SetFlightMode(ShipFlightMode mode)
				{
					mFlightMode = mode;
				}

				ShipFlightMode Ship::GetFlightMode()
				{
					return mFlightMode;
				}
			}
		}
	}
}