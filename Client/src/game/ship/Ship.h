#pragma once

#include "../../scene/BaseEntity.h"
#include "ShipTypeDef.h"
#include "ShipComponent.h"
#include "ShipFlightMode.h"
#include "ShipAutopilotStage.h"
#include <map>

namespace Exoterra
{
	namespace Client
	{
		namespace Game
		{
			namespace Ship
			{

				class Ship : public Scene::BaseEntity
				{
				private:

				protected:

					std::map<int, ShipComponentSlot *> mSlots;
					std::map<int, ShipComponent *> mComponents;

					ShipFlightMode mFlightMode;
					ShipAutopilotStage mAutopilotStage;
					Math::Vector2 mAutoTarget;

				public:
					Ship(const ShipTypeDef *def, Math::Vector2 &pos);
					~Ship();

					void SetFlightMode(ShipFlightMode mode);
					ShipFlightMode GetFlightMode();
				};

			}
		}
	}
}