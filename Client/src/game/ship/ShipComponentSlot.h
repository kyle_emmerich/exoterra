#pragma once

#include "ShipComponentType.h"
#include "../../math/Vector2.h"
#include <string>

namespace Exoterra
{
	namespace Client
	{
		namespace Game
		{
			namespace Ship
			{

				typedef struct
				{
					std::string sName;
					int sID;

					ShipComponentType sType;
					int sMaxSize;
					int sInterfaceType;
					int sFormFactor;
					
					Math::Vector2 sPhysLocation;
					Math::Vector2 sPhysSize;
				} ShipComponentSlot;
			
			}
		}
	}
}