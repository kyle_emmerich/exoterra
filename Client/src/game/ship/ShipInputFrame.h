#pragma once

namespace Exoterra
{
	namespace Client
	{
		namespace Game
		{
			namespace Ship
			{

				struct ShipInputFrame
				{
					bool sForward;
					bool sLeft;
					bool sRight;

					bool sAutopilot;
					int sAutopilotX;
					int sAutopilotY;
				};

			}
		}
	}
}