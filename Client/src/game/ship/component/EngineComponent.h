#pragma once

#include "../ShipComponent.h"

namespace Exoterra
{
	namespace Client
	{
		namespace Game
		{
			namespace Ship
			{
				
				class EngineComponent : public ShipComponent
				{
				private:

				protected:
					float mThrustPower;
					float mTurnPower;
				public:
					EngineComponent(ShipComponentDef *def, int ID);
					~EngineComponent();

					float GetThrustPower();
					float GetTurnPower();
				};

			}
		}
	}
}