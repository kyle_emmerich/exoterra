#include "EngineComponent.h"
#include <iostream>
#include "../../../util/ConversionUtility.h"

namespace Exoterra
{
	namespace Client
	{
		namespace Game
		{
			namespace Ship
			{

				EngineComponent::EngineComponent(ShipComponentDef *def, int ID)
					: ShipComponent(def, ID)
				{
					//We need to extract our data from the def. It should contain
					//two floats, so we'll need to test the size.
					size_t desired = sizeof(float) * 2;
					
					if (desired != def->sDataSize)
					{
						std::cout<<"Invalid Engine component loaded."<<std::endl;
						return;
						//Handle this gracefully somehow
					}

					Util::FloatConvert helper;
					helper.bytes[0] = def->sData[0];
					helper.bytes[1] = def->sData[1];
					helper.bytes[2] = def->sData[2];
					helper.bytes[3] = def->sData[3];
					mThrustPower = helper.value;

					helper.bytes[0] = def->sData[4];
					helper.bytes[1] = def->sData[5];
					helper.bytes[2] = def->sData[6];
					helper.bytes[3] = def->sData[7];
					mTurnPower = helper.value;
				}

				float EngineComponent::GetThrustPower()
				{
					return mThrustPower;
				}

				float EngineComponent::GetTurnPower()
				{
					return mTurnPower;
				}

			}
		}
	}
}