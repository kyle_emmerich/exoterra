#pragma once

namespace Exoterra
{
	namespace Client
	{
		namespace Game
		{
			namespace Ship
			{

				enum ShipComponentType
				{
					SCT_Invalid = 0,
					SCT_Engine,
					SCT_Reactor,
					//to be continued...
				};
			
			}
		}
	}
}
