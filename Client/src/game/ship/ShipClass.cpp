#include "ShipClass.h"

namespace Exoterra
{
	namespace Client
	{
		namespace Game
		{
			namespace Ship
			{
				std::string GetShipClassString(ShipClass e)
				{
					switch (e)
					{
					case SC_Invalid:
						return "Invalid";
					case SC_Freighter:
						return "Freighter";
					case SC_Transport:
						return "Transport";
					case SC_Barge:
						return "Barge";
					case SC_CargoLiner:
						return "Cargo Liner";
					case SC_PassengerLiner:
						return "Passenger Liner";
					case SC_Fighter:
						return "Fighter";
					case SC_Interceptor:
						return "Interceptor";
					case SC_LongRangeFighter:
						return "Long-range Fighter";
					case SC_Recon:
						return "Recon";
					case SC_Frigate:
						return "Frigate";
					case SC_Support:
						return "Support";
					case SC_Patrol:
						return "Patrol";
					case SC_Destroyer:
						return "Destroyer";
					case SC_Cruiser:
						return "Cruiser";
					case SC_Battlecruiser:
						return "Battlecruiser";
					case SC_Battleship:
						return "Battleship";
					case SC_Carrier:
						return "Carrier";
					case SC_Dreadnought:
						return "Dreadnought";
					case SC_MobileWarPlatform:
						return "Mobile War Platform";
					default:
						return "Invalid";
					}
					return "Invalid";
				}

			}
		}
	}
}