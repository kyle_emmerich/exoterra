#include "BaseEntity.h"
#include <iostream>

namespace Exoterra
{
	namespace Client
	{
		namespace Scene
		{
			BaseEntity::BaseEntity(Texture *texture, Math::Vector2 position, float mass)
				: mTexture(texture), mPos(position), mVelocity(0.0f, 0.0f), mAcceleration(0.0f, 0.0f), mAngle(0.0f), mMass(mass)
			{			
			}

			BaseEntity::~BaseEntity()
			{
			}

			void BaseEntity::Render(Rendering::RectRenderer *pRenderer, float *projection, Math::Vector2 &camPos)
			{
				Math::Vector2 adjustedPos = mPos - camPos;
				pRenderer->DrawRect(mTexture, adjustedPos, Math::Vector2(mTexture->Width * 0.5f, mTexture->Height * 0.5f), projection, mAngle);
			}

			void BaseEntity::Update(float delta)
			{
				mVelocity += mAcceleration * delta;
				mAcceleration.Set(0.0f, 0.0f);
				mPos += mVelocity;
			}

			void BaseEntity::LocalApplyForce(Math::Vector2 &force)
			{
				mAcceleration += (force / mMass);
			}

			void BaseEntity::LocalSetAngle(float angle)
			{
				mAngle = angle;
			}

			Math::Vector2 &BaseEntity::GetPosition()
			{
				return mPos;
			}
		}
	}
}