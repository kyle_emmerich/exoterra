#pragma once
#include "../../math/Vector2.h"
#include "../BaseEntity.h"
#include <vector>

namespace Exoterra
{
	namespace Client
	{
		namespace Scene
		{
			class QuadTree
			{
			protected:
				static const int MAX_ENTITIES_PER_NODE = 8;

				Math::Vector2 mSize;
				Math::Vector2 mPos;

				QuadTree* mNodes[2][2];
				std::vector<BaseEntity*> mEntities;
			public:
				QuadTree(Math::Vector2 &size);
				QuadTree(Math::Vector2 &size, Math::Vector2 &postition);

				~QuadTree();

				bool AddEntity(BaseEntity* pEntity);
				bool RemoveEntity(BaseEntity* pEntity);

				QuadTree* LocatePoint(Math::Vector2 &location);
			};
		}
	}
}
