#include "QuadTree.h"

namespace Exoterra
{
	namespace Client
	{
		namespace Scene
		{
			QuadTree::QuadTree(Math::Vector2 &size)
				: mSize(size), mPos(0.0f, 0.0f)
			{
				mNodes[0][0] = 0;
				mNodes[0][1] = 0;
				mNodes[1][0] = 0;
				mNodes[1][1] = 0;
			}

			QuadTree::QuadTree(Math::Vector2 &size, Math::Vector2 &position)
				: mSize(size), mPos(position)
			{
				mNodes[0][0] = 0;
				mNodes[0][1] = 0;
				mNodes[1][0] = 0;
				mNodes[1][1] = 0;
			}

			QuadTree::~QuadTree()
			{
				delete mNodes[0][0];
				delete mNodes[0][1];
				delete mNodes[1][0];
				delete mNodes[1][1];
			}

			bool QuadTree::AddEntity(BaseEntity* pEntity)
			{
				if(mNodes[0][0] != NULL) // we have Nodes
				{
					Math::Vector2 pos = pEntity->GetPosition();

					int x = 0;
					if(pos.GetX() > mPos.GetX() + mSize.GetX()/2)
					{
						x = 1;
					}

					int y = 0;
					if(pos.GetY() > mPos.GetY() + mSize.GetY()/2)
					{
						y = 1;
					}

					return mNodes[x][y]->AddEntity(pEntity);
				}
				if(mEntities.size() == MAX_ENTITIES_PER_NODE)
				{
					mNodes[0][0] = new QuadTree(mSize * 0.5f, mPos);
					mNodes[0][1] = new QuadTree(mSize * 0.5f, Math::Vector2(mPos.GetX(), mPos.GetY() + mSize.GetY()/2));
					mNodes[1][0] = new QuadTree(mSize * 0.5f, Math::Vector2(mPos.GetX() + mSize.GetX()/2, mPos.GetY()));
					mNodes[1][1] = new QuadTree(mSize * 0.5f, mPos + mSize*0.5f);
					for(std::vector<BaseEntity*>::size_type i = 0; i != mEntities.size(); i++)
					{
						this->AddEntity(mEntities[i]);
					}
					mEntities.clear();
					this->AddEntity(pEntity);
					return true;
				}
				mEntities.push_back(pEntity);
				return false;
			}

			bool QuadTree::RemoveEntity(BaseEntity *pEntity)
			{
				if (mNodes[0][0] != 0)
				{
					Math::Vector2 pos = pEntity->GetPosition();

					int x = 0;
					if(pos.GetX() > mPos.GetX() + mSize.GetX()/2)
					{
						x = 1;
					}

					int y = 0;
					if(pos.GetY() > mPos.GetY() + mSize.GetY()/2)
					{
						y = 1;
					}

					return mNodes[x][y]->RemoveEntity(pEntity);
				}
				std::vector<BaseEntity*>::iterator it;
				bool found = false;
				for (it = mEntities.begin(); it != mEntities.end(); it++)
				{
					if ((*it) == pEntity)
					{
						mEntities.erase(it, it);
						found = true;
						break;
					}
				}
				
				if (found)
				{
					return false;
				}
				else
				{
					
				}
			}

			QuadTree* QuadTree::LocatePoint(Math::Vector2 &location)
			{
				if(mNodes[0][0] != 0) // we have Nodes
				{
					int x = 0;
					if(location.GetX() > location.GetX() + mSize.GetX()/2)
					{
						x = 1;
					}

					int y = 0;
					if(location.GetY() > location.GetY() + mSize.GetY()/2)
					{
						y = 1;
					}

					return mNodes[x][y]->LocatePoint(location);
				}
				return this;
			}
		}
	}
}
