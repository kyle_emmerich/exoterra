#pragma once

#include "../rendering/RectRenderer.h"
#include "../math/Vector2.h"

namespace Exoterra
{
	namespace Client
	{
		namespace Scene
		{
			class BaseEntity
			{
			private:

			protected:

				Math::Vector2 mPos;
				Math::Vector2 mVelocity;
				Math::Vector2 mAcceleration;

				float mAngle;

				float mMass;

				Texture *mTexture;

			public:
				BaseEntity(Texture *texture, Math::Vector2 position, float mass);
				~BaseEntity();

				void Render(Rendering::RectRenderer *pRenderer, float *projection, Math::Vector2 &camPos);
				void Update(float delta);

				void LocalApplyForce(Math::Vector2 &force);
				void LocalSetAngle(float angle);

				Math::Vector2 &GetPosition();
			};
		}
	}
}