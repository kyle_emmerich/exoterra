#include <iostream>
#include <GL/glew.h>
#include <GL/glfw.h>

#include "gwen/Gwen.h"
#include "gwen/skins/Simple.h"
#include "gwen/skins/TexturedBase.h"
#include "gwen/Input/Windows.h"

#include "gwen/Controls/WindowControl.h"
#include "gwen/Controls.h"

#include "Gwen/Renderers/OpenGL_DebugFont.h"
#include "GwenRenderer.h"

#include "ui/LoginTrail.h"
#include "ui/DebugTrail.h"

#include "rendering/RectRenderer.h"
#include "rendering/Camera.h"
#include "rendering/Background.h"

#include "util/FileUtility.h"
#include "util/TextureLoader.h"

#include "game/ship/LocalShip.h"


using namespace std;

using namespace Exoterra;
using namespace Client;
using namespace Game;

Gwen::Controls::Canvas *g_pCanvas;

int main(int argc, char *argv[])
{
	bool running = true;
	if (!glfwInit())
	{
		exit(0);
	}
	//After initializing GLFW, we have it open a window for us.
	if (!glfwOpenWindow(1500, 1000, 0, 0, 0, 0, 0, 0, GLFW_WINDOW))
	{
		glfwTerminate();
		exit(0);
	}
	//Now set its title to none other than...
	glfwSetWindowTitle("Exoterra");
	glfwEnable(GLFW_MOUSE_CURSOR);

	//Maximize the window under Win32
	HWND wnd = FindWindow("GLFW27", "Exoterra");
	ShowWindow(wnd, SW_MAXIMIZE);

	//This will have to change once we get FreeType to work.
	//Maybe move to our own GUI manager?
	Gwen::Renderer::OpenGL_DebugFont *pRenderer = new Gwen::Renderer::OpenGL_DebugFont;
	//Exoterra::Client::GwenRenderer *pRenderer = new Exoterra::Client::GwenRenderer;
	Gwen::Skin::TexturedBase skin;
	skin.SetDefaultFont(L"assets/fonts/MavenPro-Regular.ttf");
	skin.SetRender(pRenderer);
	skin.Init("DefaultSkin.png");

	Gwen::Controls::Canvas *pCanvas = new Gwen::Controls::Canvas(&skin);
	pCanvas->SetSize(1680,1050);
	pCanvas->SetDrawBackground(false);
	g_pCanvas = pCanvas;

	Gwen::Input::Windows GwenInput;
	GwenInput.Initialize(g_pCanvas);

	Exoterra::Client::UI::DebugTrail debug;
	debug.Initialize(pCanvas);
	debug.Show(false);
	debug.GetPerfGraph()->SetXRange(0.0f, 10.0f);
	debug.GetPerfGraph()->SetYRange(0.0f, 10.0f);
	debug.GetPerfGraph()->SetXStep(1.0f);
	debug.GetPerfGraph()->SetYStep(1.0f);

	glfwDisable(GLFW_AUTO_POLL_EVENTS);

	if (glewInit() != 0)
	{
		std::cout<<"Couldn't init GLEW"<<std::endl;
		return 0;
	}

	MSG msg;

	float ortho[16];
	float trans[16] = {
		1.0f, 0.0f, 0.0f, 100.0f,
		0.0f, 1.0f, 0.0f, 100.0f,
		0.0f, 0.0f, 1.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f
	};

	Exoterra::Client::Rendering::RectRenderer rc;
	std::string vs = Exoterra::Client::Util::GetTextFile("assets/shaders/base.v.glsl");
	std::string fs = Exoterra::Client::Util::GetTextFile("assets/shaders/base.f.glsl");
	rc.Initialize(vs, fs);

	Exoterra::Client::Util::TextureLoader loader;
	Exoterra::Client::Texture *tex = loader.LoadPNG("assets/img/ship1.png");
	Texture *banana = loader.LoadPNG("assets/img/test.png");
	Texture *farBG = loader.LoadPNG("assets/img/stars_far.png");
	Texture *nearBG = loader.LoadPNG("assets/img/stars_near.png");

	float ang = 0.0f;

	Ship::ShipTypeDef *def = new Ship::ShipTypeDef;
	def->sTypeName = "Test";
	def->sTypeID = 0;

	def->sClass = Ship::SC_Fighter;
	def->sTexture = tex;
	def->sMass = 1.0f;

	def->sNumComponentSlots = 0;
	//def->sComponentSlots = 0;

	Ship::LocalShip ship(def, Exoterra::Math::Vector2(0.0f, 0.0f));
	Ship::ShipInputFrame input;

	Rendering::Camera cam;
	cam.SetCenter(ship.GetPosition());

	Scene::BaseEntity test_ent(banana, Math::Vector2(100.0f, 100.0f), 0.0f);

	Rendering::Background bg(farBG, nearBG, 0.5f);
	bg.Initialize(Util::GetTextFile("assets/shaders/background.v.glsl"),
				  Util::GetTextFile("assets/shaders/background.f.glsl"));

	double start = glfwGetTime();
	double end = glfwGetTime();
	double delta = 0.0f;

	Math::Vector2 camPos;

	int frame_id = 0;
	int ctr = 0;
	float running_sum = 0.0f;


	while (running)
	{
		int w, h;
		glfwGetWindowSize(&w, &h);

		//Gwen uses OpenGL 2.0 for some reason, so
		//we need to set up the matrix stack for her.
		//This is done every frame so we can easily
		//resize the window whenever.
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glOrtho(0, w, h, 0, -1.0, 1.0);
		glMatrixMode( GL_MODELVIEW );
		glViewport(0, 0, w, h);
		g_pCanvas->SetSize(w, h);

		camPos = cam.GetCamPos(w, h);

		//Now clear it...
		glClear(GL_COLOR_BUFFER_BIT);

		if (PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE))
		{
			//Let GLFW do its stuff with the message.
			glfwPollEvents();

			//Now we need to get the WM_CHARs out of it.
			//I don't think GLFW ever calls TranslateMessage,
			//which I assume is what prompts Windows to send
			//a WM_CHAR message, which is used by Gwen for text input.
			TranslateMessage(&msg);
			GwenInput.ProcessMessage(msg);
			//Don't question it, it works.

			if (glfwGetKey(GLFW_KEY_F2) != 0)
			{
				debug.Show(true);
			}
		}

		input.sForward = (glfwGetKey('W') == GLFW_PRESS ? true : false);
		input.sLeft = (glfwGetKey('A') == GLFW_PRESS ? true : false);
		input.sRight = (glfwGetKey('D') == GLFW_PRESS ? true : false);

		input.sAutopilot = false;
		if (glfwGetMouseButton(GLFW_MOUSE_BUTTON_MIDDLE) && ship.GetFlightMode() != Ship::SFM_Autopilot)
		{
			input.sAutopilot = true;
			glfwGetMousePos(&input.sAutopilotX, &input.sAutopilotY);
			input.sAutopilotX += camPos.GetX();
			input.sAutopilotY += camPos.GetY();
		}

		cam.SetCenter(ship.GetPosition());

		//Render our scene.
		glGetFloatv(GL_PROJECTION_MATRIX, ortho);

		bg.Render(camPos, w, h, ortho, 0.866f, 0.866f, 0.066f, 1.0f);

		ship.Render(&rc, ortho, camPos);
		ship.Update((float)delta, &input);

		//test_ent.Render(&rc, ortho, camPos);

		//Render the Gwen canvas on top of everything.
		pCanvas->RenderCanvas();
		//And show it.
		glfwSwapBuffers();

		//Find out if we need to keep going.
		running = (glfwGetWindowParam(GLFW_OPENED) != 0) ? (true) : (false);
		end = glfwGetTime();
		delta = (end - start);

		frame_id++;
		running_sum += delta;
		if (frame_id >= 60)
		{
			frame_id = 0;
			ctr++;
			if (ctr >= 10) ctr = 0;

			float avg = running_sum / 60.0f;
			running_sum = 0.0f;
			debug.GetPerfGraph()->AddDataPoint(Math::Vector2(ctr, avg * 1000.0f));
		}

		start = glfwGetTime();
	}

	loader.DeleteTexture(tex);

	//and kill GLFW.
	glfwTerminate();
	return 0;
}