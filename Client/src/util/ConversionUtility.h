#pragma once

namespace Exoterra
{
	namespace Util
	{

		union FloatConvert
		{
			float value;
			unsigned char bytes[4];
		};

		union IntConvert
		{
			int value;
			unsigned char bytes[4];
		};

	}
}