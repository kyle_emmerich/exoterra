#pragma once
#include <string>
#include "../Texture.h"

namespace Exoterra
{
	namespace Client
	{
		namespace Util
		{

			class TextureLoader
			{
			protected:

			public:
				TextureLoader();
				~TextureLoader();

				Exoterra::Client::Texture *LoadPNG(std::string fileName);

				void DeleteTexture(Exoterra::Client::Texture *texture);
			};
		}
	}
}