#include "FileUtility.h"

namespace Exoterra
{
	namespace Client
	{
		namespace Util
		{
			std::string GetTextFile(std::string fileName)
			{
				std::string val;
				std::ifstream f(fileName.c_str());
				if (!f.is_open())
					return "";
				
				while (!f.eof())
				{
					std::string buf;
					std::getline(f, buf);
					val += buf + "\r\n";
				}
				return val;
			}
		}
	}
}