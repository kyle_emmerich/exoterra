#pragma once

#include <fstream>
#include <string>

namespace Exoterra
{
	namespace Client
	{
		namespace Util
		{
			std::string GetTextFile(std::string fileName);
		}
	}
}