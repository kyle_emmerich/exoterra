#include "TextureLoader.h"
#include "GL/glew.h"
#include <fstream>
#include <iostream>
#include <FreeImage.h>

namespace Exoterra
{
	namespace Client
	{
		namespace Util
		{

			TextureLoader::TextureLoader()
			{
			}

			TextureLoader::~TextureLoader()
			{
			}

			Exoterra::Client::Texture *TextureLoader::LoadPNG(std::string fileName)
			{
				Texture *newTex = new Texture;
				newTex->IsValid = true;

				const char *c_fName = fileName.c_str();
				FREE_IMAGE_FORMAT imageFmt = FreeImage_GetFileType(c_fName);
	
				if (imageFmt == FIF_UNKNOWN)
					imageFmt = FreeImage_GetFIFFromFilename(c_fName);

				if (imageFmt == FIF_UNKNOWN)
				{
					newTex->IsValid = false;
					return newTex;
				}

				FIBITMAP *bits = FreeImage_Load(imageFmt, c_fName);

				if (!bits)
				{
					newTex->IsValid = false;
					return newTex;
				}

				FIBITMAP *bits32 = FreeImage_ConvertTo32Bits(bits);
				FreeImage_Unload(bits);
				if (!bits32)
				{
					newTex->IsValid = false;
					return newTex;
				}
				
				FreeImage_FlipVertical(bits32);

				glGenTextures(1, &newTex->GLID);
				newTex->Width = FreeImage_GetWidth(bits32);
				newTex->Height = FreeImage_GetHeight(bits32);

				glBindTexture(GL_TEXTURE_2D, newTex->GLID);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

#ifdef FREEIMAGE_BIGENDIAN
				GLenum format = GL_RGBA;
#else
				GLenum format = GL_BGRA;
#endif
				glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, newTex->Width, newTex->Height, 0, format, GL_UNSIGNED_BYTE, (const GLvoid*)FreeImage_GetBits(bits32));
				FreeImage_Unload(bits32);

				return newTex;
			}

			void TextureLoader::DeleteTexture(Exoterra::Client::Texture *texture)
			{
				if (texture->IsValid)
					glDeleteTextures(1, &texture->GLID);
				delete texture;
			}
		}
	}
}