#pragma once

namespace Exoterra
{
	namespace Math
	{
		class Vector2
		{
		private:
			float mX;
			float mY;
		public:
			Vector2(float X = 0.0f, float Y = 0.0f);
			~Vector2();

			float GetX();
			float GetY();
			void SetX(float newX);
			void SetY(float newY);
			void Set(float X, float Y) { mX = X; mY = Y; }

			float operator [](int idx);

			float GetLengthSq();
			float GetLength();

			Vector2 GetUnit();
			void Normalize();

			Vector2 operator + (Vector2 &rhs);
			Vector2 operator +=(Vector2 &rhs);
			Vector2 operator - (Vector2 &rhs);
			Vector2 operator -=(Vector2 &rhs);
			Vector2 operator * (Vector2 &rhs);
			Vector2 operator *=(Vector2 &rhs);
			Vector2 operator / (Vector2 &rhs);
			Vector2 operator /=(Vector2 &rhs);

			Vector2 operator * (float scalar);
			Vector2 operator *=(float scalar);
			Vector2 operator / (float scalar);
			Vector2 operator /=(float scalar);
		};
	}
}