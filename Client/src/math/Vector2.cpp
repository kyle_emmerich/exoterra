#include "Vector2.h"
#include <cmath>

Exoterra::Math::Vector2::Vector2(float X, float Y)
{
	mX = X;
	mY = Y;
}

Exoterra::Math::Vector2::~Vector2()
{
}

float Exoterra::Math::Vector2::GetX() 
{
	return mX;
}

float Exoterra::Math::Vector2::GetY()
{
	return mY;
}

void Exoterra::Math::Vector2::SetX(float newX)
{
	mX = newX;
}

void Exoterra::Math::Vector2::SetY(float newY)
{
	mY = newY;
}

float Exoterra::Math::Vector2::operator [](int idx)
{
	if (idx == 0)
		return mX;
	else if (idx == 1)
		return mY;
	return 0.0f;
}

float Exoterra::Math::Vector2::GetLengthSq()
{
	return (mX * mX) + (mY * mY);
}

float Exoterra::Math::Vector2::GetLength()
{
	return std::sqrt(GetLengthSq());
}

Exoterra::Math::Vector2 Exoterra::Math::Vector2::GetUnit()
{
	float len = GetLength();
	float x = mX / len;
	float y = mY / len;
	return Vector2(x, y);
}

void Exoterra::Math::Vector2::Normalize()
{
	float len = GetLength();
	mX /= len;
	mY /= len;
}

Exoterra::Math::Vector2 Exoterra::Math::Vector2::operator + (Vector2 &rhs)
{
	return Exoterra::Math::Vector2(mX + rhs.mX, mY + rhs.mY);
}

Exoterra::Math::Vector2 Exoterra::Math::Vector2::operator +=(Vector2 &rhs)
{
	mX += rhs.mX;
	mY += rhs.mY;
	return *this;
}

Exoterra::Math::Vector2 Exoterra::Math::Vector2::operator - (Vector2 &rhs)
{
	return Vector2(mX - rhs.mX, mY - rhs.mY);
}

Exoterra::Math::Vector2 Exoterra::Math::Vector2::operator -=(Vector2 &rhs)
{
	mX -= rhs.mX;
	mY -= rhs.mY;
	return *this;
}

Exoterra::Math::Vector2 Exoterra::Math::Vector2::operator * (Vector2 &rhs)
{
	return Exoterra::Math::Vector2(mX * rhs.mX, mY * rhs.mY);
}

Exoterra::Math::Vector2 Exoterra::Math::Vector2::operator *=(Vector2 &rhs)
{
	mX *= rhs.mX;
	mY *= rhs.mY;
	return *this;
}

Exoterra::Math::Vector2 Exoterra::Math::Vector2::operator / (Vector2 &rhs)
{
	return Exoterra::Math::Vector2(mX / rhs.mX, mY / rhs.mY);
}

Exoterra::Math::Vector2 Exoterra::Math::Vector2::operator /=(Vector2 &rhs)
{
	mX /= rhs.mX;
	mY /= rhs.mY;
	return *this;
}

Exoterra::Math::Vector2 Exoterra::Math::Vector2::operator * (float scalar)
{
	return Exoterra::Math::Vector2(mX * scalar, mY * scalar);
}

Exoterra::Math::Vector2 Exoterra::Math::Vector2::operator *=(float scalar)
{
	mX *= scalar;
	mY *= scalar;
	return *this;
}

Exoterra::Math::Vector2 Exoterra::Math::Vector2::operator / (float scalar)
{
	return Exoterra::Math::Vector2(mX / scalar, mY / scalar);
}

Exoterra::Math::Vector2 Exoterra::Math::Vector2::operator /=(float scalar)
{
	mX /= scalar;
	mY /= scalar;
	return *this;
}