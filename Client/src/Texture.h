#pragma once

namespace Exoterra
{
	namespace Client
	{
		typedef struct
		{
			unsigned int GLID;
			int Width;
			int Height;
			bool IsValid;
		} Texture;
	}
}