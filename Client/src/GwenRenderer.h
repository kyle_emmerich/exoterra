#pragma once

#include "gwen/Gwen.h"
#include "gwen/Renderers/OpenGL.h"
#include "ft2build.h"
#include FT_FREETYPE_H

#include <map>



namespace Exoterra
{
	namespace Client
	{
		class GwenRenderer : public Gwen::Renderer::OpenGL
		{
		protected:
			FT_Library mFTLibrary;

			typedef struct
			{
				int CharsPerRow;
				int Advances[95];
				Gwen::Texture *pTexture;
			} FontData;
			std::map<Gwen::Font*, FontData*> mFonts;
		public:
			GwenRenderer();
			~GwenRenderer();

			void LoadFont(Gwen::Font *pFont);
			void FreeFont(Gwen::Font *pFont);

			void RenderText(Gwen::Font *pFont, Gwen::Point pos, const Gwen::UnicodeString &text);
			Gwen::Point MeasureText(Gwen::Font *pFont, const Gwen::UnicodeString &text);
		};
	}
}