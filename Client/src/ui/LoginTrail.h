#pragma once
#include "InterfaceTrail.h"
#include "Gwen/Gwen.h"
#include "Gwen/Controls.h"
#include "Gwen/Events.h"

namespace Exoterra
{
	namespace Client
	{
		namespace UI
		{
			class LoginTrail : public InterfaceTrail
			{
			private:
				
			protected:
				
				Gwen::Controls::Button *mLoginButton;
				Gwen::Controls::Button *mCancelButton;

			public:
				LoginTrail();
				~LoginTrail();

				void Initialize(Gwen::Controls::Canvas *pCanvas);
				void Terminate();

				//Events
				void OnLoginPress(Gwen::Controls::Base *control);
			};
		}
	}
}