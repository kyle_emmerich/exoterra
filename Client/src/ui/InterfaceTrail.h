#pragma once

#include "Gwen/Gwen.h"

namespace Exoterra
{
	namespace Client
	{
		namespace UI
		{
			class InterfaceTrail : public Gwen::Event::Handler
			{
			private:

			protected:
				Gwen::Controls::Canvas *mCanvas;
			public:
				InterfaceTrail();
				~InterfaceTrail();

				virtual void Initialize(Gwen::Controls::Canvas *pCanvas);
				virtual void Terminate();
			};
		}
	}
}