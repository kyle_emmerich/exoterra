#include "LoginTrail.h"
#include <iostream>

namespace Exoterra
{
	namespace Client
	{
		namespace UI
		{
			LoginTrail::LoginTrail()
			{
			}

			LoginTrail::~LoginTrail()
			{
			}

			void LoginTrail::Initialize(Gwen::Controls::Canvas *pCanvas)
			{
				mLoginButton = new Gwen::Controls::Button(pCanvas);
				mLoginButton->SetBounds(10, 10, 100, 50);
				mLoginButton->SetText("Login");
				mLoginButton->onUp.Add(this, &LoginTrail::OnLoginPress);
			}

			void LoginTrail::Terminate()
			{

			}

			
			void LoginTrail::OnLoginPress(Gwen::Controls::Base *control)
			{
				Gwen::Controls::Button *button = (Gwen::Controls::Button*)control;
				button->SetDisabled(true);
			}

		}
	}
}