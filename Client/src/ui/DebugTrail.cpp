#include "DebugTrail.h"
#include <iostream>
#include "GL/glfw.h"

namespace Exoterra
{
	namespace Client
	{
		namespace UI
		{
			DebugTrail::DebugTrail()
			{
			}

			DebugTrail::~DebugTrail()
			{
			}

			void DebugTrail::Initialize(Gwen::Controls::Canvas *pCanvas)
			{
				InterfaceTrail::Initialize(pCanvas);

				mWindow = new Gwen::Controls::WindowControl(mCanvas);
				mWindow->SetBounds(100, 100, 600, 400);
				mWindow->SetTitle("Debug Info");
				mWindow->SetClosable(false);
				//mWindow->DisableResizing();

				mTabs = new Gwen::Controls::TabControl(mWindow);
				mTabs->Dock(Gwen::Pos::Fill);
				mTabs->SetTabStripPosition(Gwen::Pos::Left);
				mTabs->SetMargin(Gwen::Margin(5, 5, 5, 30));
				mGeneralTab = mTabs->AddPage("General");
				mPerformanceTab = mTabs->AddPage("Performance");
				mTabs->AddPage("Rendering");
				mNetworkingTab = mTabs->AddPage("Networking");

				mHideButton = new Gwen::Controls::Button(mWindow);
				mHideButton->SetText("Hide");
				mHideButton->SetBounds(520, 340, 55, 25);
				mHideButton->onUp.Add(this, &DebugTrail::OnHideClick);

				mGeneralPage = mGeneralTab->GetPage();
				
				mCloseButton = new Gwen::Controls::Button(mGeneralPage);
				mCloseButton->SetBounds(0, 0, 75, 25);
				mCloseButton->SetText("Close Game");
				mCloseButton->onUp.Add(this, &DebugTrail::OnCloseClick);

				mPerformancePage = mPerformanceTab->GetPage();
				
				mGraphLabel = new Gwen::Controls::Label(mPerformancePage);
				mGraphLabel->SetBounds(5, 5, 250, 20);
				mGraphLabel->SetText("Average time per frame (in ms)");
				mGraphLabel->SetAlignment(Gwen::Pos::Center);

				mPerfGraph = new Custom::GraphControl(mPerformancePage);
				mPerfGraph->SetBounds(5, 25, 250, 250);
				mPerfGraph->SetXName(Gwen::TextObject("Each space is avg. of 60 frames"));
				mPerfGraph->SetXRange(0.0f, 1000.0f);
				mPerfGraph->SetXStep(100.0f);
				mPerfGraph->SetYRange(0.0f, 50.0f);
				mPerfGraph->SetYStep(10.0f);
				mPerfGraph->SetMaxPoints(10);

				mNetworkingPage = mNetworkingTab->GetPage();

				mNetMsgLabel = new Gwen::Controls::Label(mNetworkingPage);
				mNetMsgLabel->SetBounds(5, 5, 250, 20);
				mNetMsgLabel->SetText("Incoming/Outgoing Messages");

				mNetMsgList = new Gwen::Controls::ListBox(mNetworkingPage);
				mNetMsgList->Dock(Gwen::Pos::Left);
				mNetMsgList->SetMargin(Gwen::Margin(5, 25, 5, 5));
				mNetMsgList->SetWidth(250);

				mNetIncoming = new Gwen::Controls::CheckBox(mNetworkingPage);
				mNetIncoming->SetPos(270, 30);
				mNetIncoming->SetChecked(true);
				mNetIncoming->onUp.Add(this, &DebugTrail::OnNetFilterToggle);
				mNetInLabel = new Gwen::Controls::Label(mNetworkingPage);
				mNetInLabel->SetBounds(290, 30, 200, 20);
				mNetInLabel->SetText("Show incoming messages");

				mNetOutgoing = new Gwen::Controls::CheckBox(mNetworkingPage);
				mNetOutgoing->SetPos(270, 55);
				mNetOutgoing->SetChecked(true);
				mNetOutgoing->onUp.Add(this, &DebugTrail::OnNetFilterToggle);
				mNetOutLabel = new Gwen::Controls::Label(mNetworkingPage);
				mNetOutLabel->SetBounds(290, 55, 200, 20);
				mNetOutLabel->SetText("Show outgoing messages");
				
			}

			void DebugTrail::Terminate()
			{

			}

			void DebugTrail::Show(bool isShown)
			{
				if (isShown)
					mWindow->Show();
				else
					mWindow->Hide();
			}

			void DebugTrail::OnHideClick(Gwen::Controls::Base *control)
			{
				this->Show(false);
			}

			void DebugTrail::OnNetFilterToggle(Gwen::Controls::Base *control)
			{
				if (mNetIncoming->IsChecked() && mNetOutgoing->IsChecked())
				{
					mNetMsgLabel->SetText("Incoming/Outgoing Messages");
				}
				else if (mNetIncoming->IsChecked() && !mNetOutgoing->IsChecked())
				{
					mNetMsgLabel->SetText("Incoming Messages Only");
				}
				else if (!mNetIncoming->IsChecked() && mNetOutgoing->IsChecked())
				{
					mNetMsgLabel->SetText("Outgoing Messages Only");
				}
				else
				{
					mNetMsgLabel->SetText("Please check incoming or outgoing messages.");
				}
			}

			void DebugTrail::OnCloseClick(Gwen::Controls::Base *control)
			{
				glfwCloseWindow();
			}	
		}
	}
}