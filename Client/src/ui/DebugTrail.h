#pragma once

#include "InterfaceTrail.h"
#include "Gwen/Controls.h"
#include "Gwen/Controls/WindowControl.h"
#include "Gwen/Controls/TabControl.h"
#include "Gwen/Controls/ListBox.h"
#include "Gwen/Controls/CheckBox.h"

#include "Custom/GraphControl.h"

namespace Exoterra
{
	namespace Client
	{
		namespace UI
		{

			class DebugTrail : public InterfaceTrail
			{
			private:

			protected:
				Gwen::Controls::WindowControl *mWindow;
				Gwen::Controls::TabControl *mTabs;
				Gwen::Controls::Button *mHideButton;

				Gwen::Controls::TabButton *mGeneralTab;
				Gwen::Controls::Base *mGeneralPage;
				Gwen::Controls::Button *mCloseButton;

				Gwen::Controls::TabButton *mPerformanceTab;
				Gwen::Controls::Base *mPerformancePage;
				Gwen::Controls::Label *mGraphLabel;
				Custom::GraphControl *mPerfGraph;

				Gwen::Controls::TabButton *mNetworkingTab;
				Gwen::Controls::Base *mNetworkingPage;
				Gwen::Controls::Label *mNetMsgLabel;
				Gwen::Controls::CheckBox *mNetIncoming;
				Gwen::Controls::Label *mNetInLabel;
				Gwen::Controls::CheckBox *mNetOutgoing;
				Gwen::Controls::Label *mNetOutLabel;
				Gwen::Controls::ListBox *mNetMsgList;


			public:
				DebugTrail();
				~DebugTrail();

				void Initialize(Gwen::Controls::Canvas *pCanvas);
				void Terminate();

				void Show(bool isShown);

				//Events
				void OnHideClick(Gwen::Controls::Base *control);
				void OnNetFilterToggle(Gwen::Controls::Base *control);
				void OnCloseClick(Gwen::Controls::Base *control);

				Custom::GraphControl *GetPerfGraph() { return mPerfGraph; }
			};
		}
	}
}