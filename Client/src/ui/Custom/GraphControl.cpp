#include "GraphControl.h"
#include "Gwen/Skin.h"
#include "Gwen/Utility.h"
#include <iostream>

namespace Exoterra
{
	namespace Client
	{
		namespace UI
		{
			namespace Custom
			{
				GWEN_CONTROL_CONSTRUCTOR(GraphControl)
				{
					SetSize(250, 250);
					mStart = 0;
					mLast = 0;
					mNumPoints = 0;
				}

				void GraphControl::Render(Gwen::Skin::Base* skin)
				{
					Gwen::Renderer::Base *renderer = skin->GetRender();

					Gwen::Rect bounds = m_RenderBounds;

					//Draw a 2px border, first.
					renderer->SetDrawColor(Gwen::Color(0, 0, 0, 255));
					renderer->DrawFilledRect(bounds);

					//Now shrink our bounds.
					bounds.x += 2;
					bounds.y += 2;
					bounds.w -= 4;
					bounds.h -= 4;

					//Draw the background.
					renderer->SetDrawColor(Gwen::Color(128, 128, 128, 255));
					renderer->DrawFilledRect(bounds);
					
					//Now draw the X axis.
					renderer->SetDrawColor(Gwen::Color(255, 255, 255, 255));
					renderer->DrawFilledRect(Gwen::Rect(bounds.x, bounds.h - 20, bounds.w, 2));

					//Now, the Y axis.
					renderer->SetDrawColor(Gwen::Color(255, 255, 255, 255));
					renderer->DrawFilledRect(Gwen::Rect(bounds.x + 20, bounds.y, 2, bounds.h));

					//Draw the X axis name.
					renderer->RenderText(skin->GetDefaultFont(), Gwen::Point(bounds.x + 30, bounds.h - 15), mXName.Get());

					//Now, draw the vertical lines of the grid.
					renderer->SetDrawColor(Gwen::Color(200, 200, 200, 255));
					for (int i = 1; i < mNumXSteps; i++)
					{
						renderer->DrawFilledRect(Gwen::Rect(
							bounds.x + 20 + ((230 / mNumXSteps) * i),
							bounds.y,
							1,
							bounds.h - 22));
					}

					//Now, draw the horizontal lines of the grid.
					renderer->SetDrawColor(Gwen::Color(200, 200, 200, 255));
					for (int i = 1; i < mNumYSteps; i++)
					{
						renderer->DrawFilledRect(Gwen::Rect(
							bounds.x + 22,
							bounds.y + ((230 / mNumYSteps) * i),
							bounds.w - 22,
							1));
					}

					//Now we need to draw the data points.
					renderer->SetDrawColor(Gwen::Color(255, 50, 50));
					DataPoint *cur = mStart;
					while (cur != 0)
					{
						float x = cur->sData.GetX() / (mXRangeTop - mXRangeBottom);
						float y = cur->sData.GetY() / (mYRangeTop - mYRangeBottom);

						x *= 230.0f;
						y *= 230.0f;

						x += 22;
						y = 232 - y;

						renderer->DrawFilledRect(Gwen::Rect(
							x, y - 2,
							4, 4));

						cur = cur->sNext;
					}
				}

				void GraphControl::SetXName(Gwen::TextObject &name)
				{
					mXName = name;
				}

				void GraphControl::SetYName(Gwen::TextObject &name)
				{
					mYName = name;
				}

				void GraphControl::SetXRange(float bottom, float top)
				{
					mXRangeBottom = bottom;
					mXRangeTop = top;
				}

				void GraphControl::SetYRange(float bottom, float top)
				{
					mYRangeBottom = bottom;
					mYRangeTop = top;
				}

				void GraphControl::SetXStep(float step)
				{
					mXStep = step;
					mNumXSteps = std::floor((mXRangeTop - mXRangeBottom) / mXStep);
				}

				void GraphControl::SetYStep(float step)
				{
					mYStep = step;
					mNumYSteps = std::floor((mYRangeTop - mYRangeBottom) / mYStep);
				}

				void GraphControl::SetMaxPoints(int points)
				{
					mMaxPoints = points;
				}

				void GraphControl::AddDataPoint(Math::Vector2 &data)
				{
					mNumPoints++;
					DataPoint *newPoint = new DataPoint;
					newPoint->sData = data;
					if (mStart == 0)
					{
						mStart = newPoint;
						mLast = newPoint;
						newPoint->sPrev = 0;
						newPoint->sNext = 0;
					} else {
						mLast->sNext = newPoint;
						newPoint->sPrev = mLast;
						mLast = newPoint;
						newPoint->sNext = 0;
					}

					if (mNumPoints > mMaxPoints)
					{
						mStart = mStart->sNext;
						delete mStart->sPrev;
						mStart->sPrev = 0;

						mNumPoints--;
					}
				}
			}
		}
	}
}