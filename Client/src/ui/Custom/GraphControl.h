#pragma once

#include "Gwen/Controls/Base.h"
#include "Gwen/TextObject.h"
#include "../../math/Vector2.h"

namespace Exoterra
{
	namespace Client
	{
		namespace UI
		{
			namespace Custom
			{

				class GraphControl : public Gwen::Controls::Base
				{
				protected:
					Gwen::TextObject mXName;
					Gwen::TextObject mYName;

					float mXRangeBottom;
					float mXRangeTop;
					float mYRangeBottom;
					float mYRangeTop;
					float mXStep;
					float mYStep;
					int mNumXSteps;
					int mNumYSteps;

					struct DataPoint
					{
						Math::Vector2 sData;
						DataPoint *sNext;
						DataPoint *sPrev;
					};

					DataPoint *mStart;
					DataPoint *mLast;
					int mNumPoints;
					int mMaxPoints;
				public:
					GWEN_CONTROL(GraphControl, Gwen::Controls::Base);

					void Render(Gwen::Skin::Base* skin);

					void SetXName(Gwen::TextObject &name);
					void SetYName(Gwen::TextObject &name);

					void SetXRange(float bottom, float top);
					void SetYRange(float bottom, float top);

					void SetXStep(float step);
					void SetYStep(float step);

					void SetMaxPoints(int points);

					void AddDataPoint(Math::Vector2 &data);
				};
			}
		}
	}
}